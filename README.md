Readonly Storage
================

A simple read-only wrapper around a regular [ZODB](https://github.com/zopefoundation/ZODB) storage.

## Why would I want to use readonlystorage?

When you need read-only access to another storage. Let's imagine a real-life example with a web application.
You have a `filestorage` behind a [ZEO](https://github.com/zopefoundation/ZEO) server, replicated to another (read-only) `ZEO` server using [ZRS](https://github.com/zopefoundation/zc.zrs).
To maintain good performance on your primary database, you decide to only route the requests that write data to the read-write database (for example the `POST` requests), and route all 
other requests to the read-only database (for instance the `GET` requests).

Now you want this setup to be simulated in your unit tests, and you want to use a [DemoStorage](http://www.zodb.org/en/latest/reference/storages.html#demostorage) in those tests to preserve
good execution times. Even if `DemoStorage` provides a way to wrap another storage, it cannot be made read-only. Here you can use a `readonlystorage` to wrap a `DemoStorage`, making it
the perfect read-only replication of you primary storage.

## Usage example

```python
import ZODB

# Let's set a read-write DemoStorage
rw_storage = ZODB.DemoStorage.DemoStorage()
rw_db = ZODB.DB(rw_storage, cache_size=0)

# And now, let's set a ReadOnlyStorage around it.
ro_storage = readonlystorage.ReadOnlyStorage(rw_storage)
ro_db = ZODB.DB(ro_storage)

# Now, add some data
rw_conn = rw_db.open()
rw_conn.root()["foo"] = "bar"
rw_conn.transaction_manager.commit()
rw_conn.close()

# Read the data
ro_conn = ro_db.open()
assert "bar" == ro_conn.root()["foo"]

ro_conn.root()["foo"] = "anything"
ro_conn.transaction_manager.commit() # This one would emit a ZODB.POSException.ReadOnlyError!

ro_conn.close()
```

Note that `cache_size=0` is needed to make `tests/test_readonly_storage.py::TestReadOnlyStorage::tes_successive_transactions` work.
